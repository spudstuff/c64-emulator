using System;
using System.IO;

namespace C64
{
    public class C64
    {
        public delegate void TraceBeforeCpuInstruction(UInt16 PC, byte IR);
        public delegate void TraceAfterCpuInstruction(byte A, byte X, byte Y, byte S, byte P);
        public delegate void CpuError(string message);
        public event TraceBeforeCpuInstruction OnTraceBeforeCpuInstruction;
        public event TraceAfterCpuInstruction OnTraceAfterCpuInstruction;
        public event CpuError OnCpuError;

        private byte A, X, Y, S, P, IR;
        private UInt16 PC;
        private const byte C = 0x01; // 0000 0001
        private const byte Z = 0x02; // 0000 0010
        private const byte I = 0x04; // 0000 0100
        private const byte D = 0x08; // 0000 1000
        private const byte B = 0x10; // 0001 0000
        private const byte V = 0x40; // 0100 0000
        private const byte N = 0x80; // 1000 0000
        private MemoryIO _memoryIo = new MemoryIO();
        private bool _errorState = false;
        private string _loadFilename = null;
        private bool _loadIntoBasic = false;
        private bool _loadRunImmediately = true;
        private int _cycles = 0;

        static C64()
        {
            Opcodes = new string[256, 2]
            {
                {"BRK","imp"}, {"ORA","inx"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"ORA","zpg"}, {"ASL","zpg"}, {"???","imp"},
                {"PHP","imp"}, {"ORA","imm"}, {"ASL","acc"}, {"???","imp"},
                {"???","imp"}, {"ORA","abs"}, {"ASL","abs"}, {"???","imp"},
                {"BPL","rel"}, {"ORA","iny"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"ORA","zpx"}, {"ASL","zpx"}, {"???","imp"},
                {"CLC","imp"}, {"ORA","aby"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"ORA","abx"}, {"ASL","abx"}, {"???","imp"},
                {"JSR","abs"}, {"AND","inx"}, {"???","imp"}, {"???","imp"},
                {"BIT","zpg"}, {"AND","zpg"}, {"ROL","zpg"}, {"???","imp"},
                {"PLP","imp"}, {"AND","imm"}, {"ROL","acc"}, {"???","imp"},
                {"BIT","abs"}, {"AND","abs"}, {"ROL","abs"}, {"???","imp"},
                {"BMI","rel"}, {"AND","iny"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"AND","zpx"}, {"ROL","zpx"}, {"???","imp"},
                {"SEC","imp"}, {"AND","aby"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"AND","abx"}, {"ROL","abx"}, {"???","imp"},
                {"RTI","imp"}, {"EOR","inx"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"EOR","zpg"}, {"LSR","zpg"}, {"???","imp"},
                {"PHA","imp"}, {"EOR","imm"}, {"LSR","acc"}, {"???","imp"},
                {"JMP","abs"}, {"EOR","abs"}, {"LSR","abs"}, {"???","imp"},
                {"BVC","rel"}, {"EOR","iny"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"EOR","zpx"}, {"LSR","zpx"}, {"???","imp"},
                {"CLI","imp"}, {"EOR","aby"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"EOR","abx"}, {"LSR","abx"}, {"???","imp"},
                {"RTS","imp"}, {"ADC","inx"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"ADC","zpg"}, {"ROR","zpg"}, {"???","imp"},
                {"PLA","imp"}, {"ADC","imm"}, {"ROR","acc"}, {"???","imp"},
                {"JMP","ind"}, {"ADC","abs"}, {"ROR","abs"}, {"???","imp"},
                {"BVS","rel"}, {"ADC","iny"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"ADC","zpx"}, {"ROR","zpx"}, {"???","imp"},
                {"SEI","imp"}, {"ADC","aby"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"ADC","abx"}, {"ROR","abx"}, {"???","imp"},
                {"???","imp"}, {"STA","inx"}, {"???","imp"}, {"???","imp"},
                {"STY","zpg"}, {"STA","zpg"}, {"STX","zpg"}, {"???","imp"},
                {"DEY","imp"}, {"???","imp"}, {"TXA","imp"}, {"???","imp"},
                {"STY","abs"}, {"STA","abs"}, {"STX","abs"}, {"???","imp"},
                {"BCC","rel"}, {"STA","iny"}, {"???","imp"}, {"???","imp"},
                {"STY","zpx"}, {"STA","zpx"}, {"STX","zpy"}, {"???","imp"},
                {"TYA","imp"}, {"STA","aby"}, {"TXS","imp"}, {"???","imp"},
                {"???","imp"}, {"STA","abx"}, {"???","imp"}, {"???","imp"},
                {"LDY","imm"}, {"LDA","inx"}, {"LDX","imm"}, {"???","imp"},
                {"LDY","zpg"}, {"LDA","zpg"}, {"LDX","zpg"}, {"???","imp"},
                {"TAY","imp"}, {"LDA","imm"}, {"TAX","imp"}, {"???","imp"},
                {"LDY","abs"}, {"LDA","abs"}, {"LDX","abs"}, {"???","imp"},
                {"BCS","rel"}, {"LDA","iny"}, {"???","imp"}, {"???","imp"},
                {"LDY","zpx"}, {"LDA","zpx"}, {"LDX","zpy"}, {"???","imp"},
                {"CLV","imp"}, {"LDA","aby"}, {"TSX","imp"}, {"???","imp"},
                {"LDY","abx"}, {"LDA","abx"}, {"LDX","aby"}, {"???","imp"},
                {"CPY","imm"}, {"CMP","inx"}, {"???","imp"}, {"???","imp"},
                {"CPY","zpg"}, {"CMP","zpg"}, {"DEC","zpg"}, {"???","imp"},
                {"INY","imp"}, {"CMP","imm"}, {"DEX","imp"}, {"???","imp"},
                {"CPY","abs"}, {"CMP","abs"}, {"DEC","abs"}, {"???","imp"},
                {"BNE","rel"}, {"CMP","iny"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"CMP","zpx"}, {"DEC","zpx"}, {"???","imp"},
                {"CLD","imp"}, {"CMP","aby"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"CMP","abx"}, {"DEC","abx"}, {"???","imp"},
                {"CPX","imm"}, {"SBC","inx"}, {"???","imp"}, {"???","imp"},
                {"CPX","zpg"}, {"SBC","zpg"}, {"INC","zpg"}, {"???","imp"},
                {"INX","imp"}, {"SBC","imm"}, {"NOP","imp"}, {"???","imp"},
                {"CPX","abs"}, {"SBC","abs"}, {"INC","abs"}, {"???","imp"},
                {"BEQ","rel"}, {"SBC","iny"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"SBC","zpx"}, {"INC","zpx"}, {"???","imp"},
                {"SED","imp"}, {"SBC","aby"}, {"???","imp"}, {"???","imp"},
                {"???","imp"}, {"SBC","abx"}, {"INC","abx"}, {"???","imp"}
            };
        }

        public C64()
        {
            Trace = false;
        }

        public static string[,] Opcodes { get; set; }

        public bool Trace { get; set; }

        public byte[] Stack
        {
            get { return _memoryIo.Stack; }
        }

        public void Start()
        {
            _errorState = false;
            A = X = Y = IR = 0;
            S = 0;
            P = 0;
            PC = PeekWord(0xFFFC);

            if (File.Exists("C:\\state64.bin"))
            {
                LoadSnapshot("C:\\state64.bin");
            }
        }

        public bool NextInstructions()
        {
            int count = 0;

            while(count++ < 32768)
            {
                // Get next instruction and increment program counter
                IR = Peek(PC++);

                if (Trace)
                {
                    DoTraceBeforeCpuInstruction();
                }

                // Execute instruction
                switch (IR)
                {
                    case 0x00: brk();             break;
                    case 0x01: ora(indx());       break;
                    case 0x05: ora(zp());         break;
                    case 0x06: asl(zp());         break;
                    case 0x08: push(P);           break;
                    case 0x09: ora(imm());        break;
                    case 0x0A: asl_A();           break;
                    case 0x0D: ora(abs());        break;
                    case 0x0E: asl(abs());        break;
                    case 0x10: bfc(N);            break;
                    case 0x11: ora(indy());       break;
                    case 0x15: ora(zpx());        break;
                    case 0x16: asl(zpx());        break;
                    case 0x1D: ora(absy());       break;
                    case 0x1E: asl(absx());       break;
                    case 0x18: setflag(C, false); break;
                    case 0x19: ora(absx());       break;
                    case 0x20: jsr(abs());        break;
                    case 0x21: and_(indx());      break;
                    case 0x24: bit(zp());         break;
                    case 0x25: and_(zp());        break;
                    case 0x26: rol(zp());         break;
                    case 0x28: P = pull();        break;
                    case 0x29: and_(imm());       break;
                    case 0x2A: rol_A();           break;
                    case 0x2C: bit(abs());        break;
                    case 0x2D: and_(abs());       break;
                    case 0x2E: rol(abs());        break;
                    case 0x30: bfs(N);            break;
                    case 0x31: and_(indy());      break;
                    case 0x35: and_(zpx());       break;
                    case 0x36: rol(zpx());        break;
                    case 0x38: setflag(C, true);  break;
                    case 0x39: and_(absy());      break;
                    case 0x3D: and_(absx());      break;
                    case 0x3E: rol(absx());       break;
                    case 0x40: rti();             break;
                    case 0x41: eor(indx());       break;
                    case 0x44:                    break;
                    case 0x45: eor(zp());         break;
                    case 0x46: lsr(zp());         break;
                    case 0x48: push(A);           break;
                    case 0x49: eor(imm());        break;
                    case 0x4A: lsr_A();           break;
                    case 0x4C: jmp(abs());        break;
                    case 0x4D: eor(abs());        break;
                    case 0x4E: lsr(abs());        break;
                    case 0x51: eor(indy());       break;
                    case 0x50: bfc(V);            break;
                    case 0x55: eor(zpx());        break;
                    case 0x56: lsr(zpx());        break;
                    case 0x58: setflag(I, false); break;
                    case 0x59: eor(absy());       break;
                    case 0x5D: eor(absx());       break;
                    case 0x5E: lsr(absx());       break;
                    case 0x60: rts();             break;
                    case 0x61: adc(indx());       break;
                    case 0x65: adc(zp());         break;
                    case 0x66: ror(zp());         break;
                    case 0x68: pla();             break;
                    case 0x69: adc(imm());        break;
                    case 0x6A: ror_A();           break;
                    case 0x6C: jmp(ind());        break;
                    case 0x6D: adc(abs());        break;
                    case 0x6E: ror(abs());        break;
                    case 0x70: bfs(V);            break;
                    case 0x71: adc(indy());       break;
                    case 0x75: adc(zpx());        break;
                    case 0x76: ror(zpx());        break;
                    case 0x78: setflag(I, true);  break;
                    case 0x79: adc(absy());       break;
                    case 0x7D: adc(absx());       break;
                    case 0x7E: ror(absx());       break;
                    case 0x81: sta(indx());       break;
                    case 0x84: sty(zp());         break;
                    case 0x85: sta(zp());         break;
                    case 0x86: stx(zp());         break;
                    case 0x88: dey();             break;
                    case 0x8A: txa();             break;
                    case 0x8C: sty(abs());        break;
                    case 0x8D: sta(abs());        break;
                    case 0x8E: stx(abs());        break;
                    case 0x90: bfc(C);            break;
                    case 0x91: sta(indy());       break;
                    case 0x94: sty(zpx());        break;
                    case 0x95: sta(zpx());        break;
                    case 0x96: stx(zpy());        break;
                    case 0x98: tya();             break;
                    case 0x99: sta(absy());       break;
                    case 0x9A: txs();             break;
                    case 0x9D: sta(absx());       break;
                    case 0xA0: ldy(imm());        break;
                    case 0xA1: lda(indx());       break;
                    case 0xA2: ldx(imm());        break;
                    case 0xA4: ldy(zp());         break;
                    case 0xA5: lda(zp());         break;
                    case 0xA6: ldx(zp());         break;
                    case 0xA8: tay();             break;
                    case 0xA9: lda(imm());        break;
                    case 0xAA: tax();             break;
                    case 0xAC: ldy(abs());        break;
                    case 0xAD: lda(abs());        break;
                    case 0xAE: ldx(abs());        break;
                    case 0xB0: bfs(C);            break;
                    case 0xB1: lda(indy());       break;
                    case 0xB4: ldy(zpx());        break;
                    case 0xB5: lda(zpx());        break;
                    case 0xB6: ldx(zpy());        break;
                    case 0xB8: setflag(V, false); break;
                    case 0xB9: lda(absy());       break;
                    case 0xBA: tsx();             break;
                    case 0xBC: ldy(absx());       break;
                    case 0xBD: lda(absx());       break;
                    case 0xBE: ldx(absy());       break;
                    case 0xC0: cpy(imm());        break;
                    case 0xC1: cmp(indx());       break;
                    case 0xC4: cpy(zp());         break;
                    case 0xC5: cmp(zp());         break;
                    case 0xC6: dec(zp());         break;
                    case 0xC8: iny();             break;
                    case 0xC9: cmp(imm());        break;
                    case 0xCA: dex();             break;
                    case 0xCC: cpy(abs());        break;
                    case 0xCD: cmp(abs());        break;
                    case 0xCE: dec(abs());        break;
                    case 0xD0: bfc(Z);            break;
                    case 0xD1: cmp(indy());       break;
                    case 0xD5: cmp(zpx());        break;
                    case 0xD6: dec(zpx());        break;
                    case 0xD8: setflag(D, false); break;
                    case 0xD9: cmp(absy());       break;
                    case 0xDC: break;
                    case 0xDD: cmp(absx());       break;
                    case 0xDE: dec(absx());       break;
                    case 0xE0: cpx(imm());        break;
                    case 0xE1: sbc(indx());       break;
                    case 0xE4: cpx(zp());         break;
                    case 0xE5: sbc(zp());         break;
                    case 0xE6: inc(zp());         break;
                    case 0xE8: inx();             break;
                    case 0xE9: sbc(imm());        break;
                    case 0xEA:                    break;
                    case 0xEC: cpx(abs());        break;
                    case 0xED: sbc(abs());        break;
                    case 0xEE: inc(abs());        break;
                    case 0xF0: bfs(Z);            break;
                    case 0xF1: sbc(indy());       break;
                    case 0xF5: sbc(zpx());        break;
                    case 0xF6: inc(zpx());        break;
                    case 0xF8: setflag(D, true);  break;
                    case 0xF9: sbc(absy());       break;
                    case 0xFD: sbc(absx());       break;
                    case 0xFE: inc(absx());       break;

                    default:   Error("Unknown instruction " + String.Format("{0:x2}", IR));      break;
                }

                if (_errorState)
                {
                    return false;
                }

                if (Trace)
                {
                    DoTraceAfterCpuInstruction();
                }
            }
            irq();

            // Have we been asked to reset and load?
            _cycles++;
            if (PC == 0xff48 && _loadFilename != null && _cycles > 50)
            {
                LoadPrg();
            }

            return true;
        }

        public State GetState()
        {
            var state = new State { IR = IR, PC = PC, A = A, X = X, Y = Y, S = S, P = P };
            return state;
        }

        public void LoadSnapshot(string filename)
        {
            var fs = new FileStream(filename, FileMode.Open);
            A = (byte)fs.ReadByte();
            P = (byte)fs.ReadByte();
            var IRH = (byte)fs.ReadByte();
            var IRL = (byte)fs.ReadByte();
            PC = (UInt16)((IRH << 8) | IRL);
            S = (byte)fs.ReadByte();
            X = (byte)fs.ReadByte();
            Y = (byte)fs.ReadByte();
            fs.Read(_memoryIo.Ram, 0, _memoryIo.Ram.Length);
            fs.Close();
        }
        
        public void ResetAndLoadPrg(string filename, bool basic, bool runImmediately)
        {
            _loadFilename = filename;
            _loadIntoBasic = basic;
            _loadRunImmediately = runImmediately;
            _cycles = 0;
            Reset();
        }

        private void LoadPrg()
        {
            var fs = File.Open(_loadFilename, FileMode.Open);
            var stream = new BinaryReader(fs);
            var prog = new byte[fs.Length];

            // Read into byte array
            int offset = 0;
            int remaining = prog.Length;
            while (remaining > 0)
            {
                int read = stream.Read(prog, offset, remaining);
                if (read <= 0)
                {
                    throw new EndOfStreamException(String.Format("End of stream reached with {0} bytes left to read", remaining));
                }
                remaining -= read;
                offset += read;
            }
            stream.Close();
            fs.Close();

            // Poke into memory
            UInt16 address;
            if (_loadIntoBasic)
            {
                address = 2048;
            }
            else
            {
                address = (UInt16)(prog[(_loadFilename.ToUpper().EndsWith(".P00") ? 26 : 0)] + (prog[(_loadFilename.ToUpper().EndsWith(".P00") ? 27 : 1)] * 256));
            }
            for (int i = (_loadFilename.ToUpper().EndsWith(".P00") ? 28 : 2); i < prog.Length; i++)
            {
                Poke(address++, prog[i]);
            }
            Poke(0x2D, lo(address));
            Poke(0x2E, hi(address));

            // Clear flags
            _loadFilename = null;
            _cycles = 0;

            if (_loadRunImmediately)
            {
                SendKeys("RUN\r");
            }
        }

        private void SendKeys(string keys)
        {
            for (int i = 0; i < keys.Length; i++)
            {
                byte buff = Peek(0xC6);
                if (buff < 10)
                {
                    byte ch = (byte)keys[i];
                    Poke((UInt16)(0x0277 + buff), ch);
                    Poke(0xC6, (byte)(buff + 1));
                }
            }
        }

        private void Error(string message)
        {
            _errorState = true;
            Console.WriteLine("");
            Console.WriteLine(message);
            if (OnCpuError != null)
            {
                OnCpuError(message);
            }
        }

        private void DoTraceBeforeCpuInstruction()
        {
            if (OnTraceBeforeCpuInstruction != null)
            {
                OnTraceBeforeCpuInstruction(PC, IR);
            }
        }

        private void DoTraceAfterCpuInstruction()
        {
            if (OnTraceAfterCpuInstruction != null)
            {
                OnTraceAfterCpuInstruction(A, X, Y, S, P);
            }
        }

        /// <summary>
        /// Peek reads a byte from memory.
        /// </summary>
        /// <param name="address">Address to peek (0x0000 to 0xFFFF).</param>
        /// <returns>Byte at requested address.</returns>
        public byte Peek(UInt16 address)
        {
            return _memoryIo.Peek(address);
        }

        /// <summary>
        /// Peek a two byte word starting at the requested address.
        /// </summary>
        /// <param name="address">Address to peek (0x0000 to 0xFFFE).</param>
        /// <returns>Word at requested address.</returns>
        private UInt16 PeekWord(UInt16 address)
        {
            if (address > 0xFFFE)
            {
                Error("PeekWord outside memory range");
            }

            return (UInt16)(Peek(address) | (Peek((UInt16)(address + 1)) << 8));
        }

        /// <summary>
        /// Poke sets a byte at a memory address.
        /// </summary>
        /// <param name="address">Address to poke (0x0000 to 0xFFFF).</param>
        /// <param name="value">Byte to set.</param>
        public void Poke(UInt16 address, byte value)
        {
            _memoryIo.Poke(address, value);
        }

        /// <summary>
        /// Reset the C64.
        /// </summary>
        public void Reset()
        {
            _errorState = false;
            A = X = Y = IR = 0;
            S = 0;
            P = 0;
            _memoryIo = new MemoryIO();
            PC = PeekWord(0xFFFC);
            setflag(I, true);
        }

        #region 6510 opcodes

        /// <summary>
        /// Load the accumulator with the value at the requested address.
        /// </summary>
        /// <param name="address">Address (0x0000 to 0xFFFF).</param>
        private void lda(UInt16 address)
        {
            A = Peek(address);

            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        /// <summary>
        /// Load the X register with the value at the requested address.
        /// </summary>
        /// <param name="address">Address (0x0000 to 0xFFFF).</param>
        private void ldx(UInt16 address)
        {
            X = Peek(address);

            setflag(Z, X == 0);
            setflag(N, X >= 0x80);
        }

        /// <summary>
        /// Load the Y register with the value at the requested address.
        /// </summary>
        /// <param name="address">Address (0x0000 to 0xFFFF).</param>
        private void ldy(UInt16 address)
        {
            Y = Peek(address);

            setflag(Z, Y == 0);
            setflag(N, Y >= 0x80);
        }

        /// <summary>
        /// Store the accumulator value at the requested address.
        /// </summary>
        /// <param name="address">Address (0x0000 to 0xFFFF).</param>
        private void sta(UInt16 address)
        {
            if (address < 0x0000 || address > 0xFFFF)
                Error("Poke outside memory range");

            Poke(address, A);
        }

        /// <summary>
        /// Store the X register value at the requested address.
        /// </summary>
        /// <param name="address">Address (0x0000 to 0xFFFF).</param>
        private void stx(UInt16 address)
        {
            if (address < 0x0000 || address > 0xFFFF)
                Error("Poke outside memory range");

            Poke(address, X);
        }

        /// <summary>
        /// Store the Y register value at the requested address.
        /// </summary>
        /// <param name="address">Address (0x0000 to 0xFFFF).</param>
        private void sty(UInt16 address)
        {
            if (address < 0x0000 || address > 0xFFFF)
                Error("Poke outside memory range");

            Poke(address, Y);
        }

        /// <summary>
        /// Immediate mode.
        /// </summary>
        /// <returns>The address of the program counter.</returns>
        private UInt16 imm()
        {
            return PC++;
        }

        /// <summary>
        /// Absolute mode.
        /// </summary>
        /// <returns>The word at the program counter.</returns>
        private UInt16 abs()
        {
            UInt16 value = PeekWord(PC);
            PC += 2;
            return value;
        }

        /// <summary>
        /// Absolute indexed by X mode.
        /// </summary>
        /// <returns>The word at the program counter, indexed by the X register.</returns>
        private UInt16 absx()
        {
            UInt16 value = (UInt16)(PeekWord(PC) + X);
            PC += 2;
            return value;
        }

        /// <summary>
        /// Absolute indexed by Y mode.
        /// </summary>
        /// <returns>The word at the program counter, indexed by the Y register.</returns>
        private UInt16 absy()
        {
            UInt16 value = (UInt16)(PeekWord(PC) + Y);
            PC += 2;
            return value;
        }

        /// <summary>
        /// Absolute indirect mode.
        /// </summary>
        /// <returns>The word referenced indirectly at the program counter.</returns>
        private UInt16 ind()
        {
            UInt16 value = PeekWord(PeekWord(PC));
            PC += 2;
            return value;
        }

        /// <summary>
        /// Zero page mode.
        /// </summary>
        /// <returns>The byte at the program counter.</returns>
        private byte zp()
        {
            return Peek(PC++);
        }

        /// <summary>
        /// Zero page indexed by X mode.
        /// </summary>
        /// <returns>The byte at the program counter, indexed by the X register.</returns>
        private byte zpx()
        {
            return (byte)(Peek(PC++) + X);
        }

        /// <summary>
        /// Zero page indexed by Y mode.
        /// </summary>
        /// <returns>The byte at the program counter, indexed by the Y register.</returns>
        private byte zpy()
        {
            return (byte)(Peek(PC++) + Y);
        }

        /// <summary>
        /// Indexed indirect X mode.
        /// </summary>
        /// <returns></returns>
        private UInt16 indx()
        {
            return PeekWord((UInt16)((byte)Peek(PC++) + X));
        }

        /// <summary>
        /// Indexed indirect Y mode.
        /// </summary>
        /// <returns></returns>
        private UInt16 indy()
        {
            UInt16 value = (UInt16)(PeekWord(Peek(PC)) + Y);
            PC++;
            return value;
        }

        /// <summary>
        /// Check to see if a specific flag is set.
        /// </summary>
        /// <param name="flag">Flag to check.</param>
        /// <returns>True if the flag is set.</returns>
        private bool flagset(byte flag)
        {
            return (P & flag) == flag;
        }

        /// <summary>
        /// Set a flag.
        /// </summary>
        /// <param name="flag">Flag to set.</param>
        /// <param name="status"></param>
        private void setflag(byte flag, bool status)
        {
            if(status)
                P = (byte)(P | flag);
            else
                P = (byte)(P & ~flag);
        }

        /// <summary>
        /// Push a byte onto the stack.
        /// </summary>
        /// <param name="b">Byte to push.</param>
        private void push(byte b)
        {
            Poke((UInt16)(0x100+S), b);
            S--;
        }

        /// <summary>
        /// Pull a byte from the stack.
        /// </summary>
        /// <returns>Byte from the top of the stack.</returns>
        private byte pull()
        {
            S++;
            return Peek((UInt16)(0x100 + S));
        }

        /// <summary>
        /// Jump execution to a new address in memory.
        /// </summary>
        /// <param name="address">Address to jump to.</param>
        private void jmp(UInt16 address)
        {
            PC = address;
        }

        private void bfs(byte flag)
        {
            if (flagset(flag))
                PC += (UInt16)((sbyte)(Peek(PC)) + 1);
            else
                PC++;
        }

        private void bfc(byte flag)
        {
            if (flagset(flag))
                PC++;
            else
                PC += (UInt16)((sbyte)(Peek(PC)) + 1);
        }

        private void jsr(UInt16 address)
        {
            PC--;
            push(hi(PC));
            push(lo(PC));
            PC = address;
        }

        private void rts()
        {
            PC = pull();
            PC += (UInt16)(pull() * 256 + 1);
        }

        private void dex()
        {
            if(X == 0)
                X = 255;
            else
                X--;

            setflag(Z, X == 0);
            setflag(N, X >= 0x80);
        }

        private void dey()
        {
            if(Y == 0)
                Y = 255;
            else
                Y--;
        
            setflag(Z,lo(Y) == 0);
            setflag(N,lo(Y) >= 0x80);
        }

        private void adc(UInt16 address)
        {
            byte val = Peek(address);
            UInt16 H = (UInt16)(A + val);
            if (flagset(C))
                H++;
            setflag(V, ((A ^ val) & 0x80) != 0x80 && ((A ^ H) & 0x80) == 0x80);
            A = (byte)H;
            setflag(C, H > 0xFF);
            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        private void sbc(UInt16 address)
        {
            byte val = Peek(address);
            UInt16 H = (UInt16)(A - val);
            if (!flagset(C))
                H--;
            setflag(V, ((A ^ val) & 0x80) == 0x80 && ((A ^ H) & 0x80) == 0x80);
            A = (byte)H;
            setflag(C, H <= 0xFF);
            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        private void tax()
        {
            X = A;
            setflag(Z, X == 0);
            setflag(N, X >= 0x80);
        }

        private void tay()
        {
            Y = A;
            setflag(Z, Y == 0);
            setflag(N, Y >= 0x80);
        }

        private void tsx()
        {
            X = S;
            setflag(Z, X == 0);
            setflag(N, X >= 0x80);
        }

        private void txa()
        {
            A = X;
            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        private void txs()
        {
            S = X;
        }

        private void tya()
        {
            A = Y;
            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        private void cmp(UInt16 address)
        {
            UInt16 H = (UInt16)(A - Peek(address));
            setflag(C, H <= 0xFF);
            setflag(Z, lo(H) == 0);
            setflag(N, lo(H) >= 0x80);
        }

        private void and_(UInt16 address)
        {
            A = (byte)(A & Peek(address));
            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        private void ora(UInt16 address)
        {
            A = (byte)(A | Peek(address));
            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        private void brk()
        {
            setflag(B, true);
            PC++;
            push(hi(PC));
            push(lo(PC));
            push(P);
            setflag(I, true);
            PC = PeekWord(0xFFFE);
        }

        private void inx()
        {
            UInt16 H = (UInt16)(X + 1);
            X = (byte)H;
            setflag(Z, X == 0);
            setflag(N, X >= 0x80);
        }

        private void iny()
        {
            UInt16 H = (UInt16)(Y + 1);
            Y = (byte)H;
            setflag(Z, Y == 0);
            setflag(N, Y >= 0x80);
        }

        private void inc(UInt16 address)
        {
            UInt16 H = (UInt16)(Peek(address) + 1);
            Poke(address, (byte)H);
            setflag(Z, lo(H) == 0);
            setflag(N, lo(H) >= 0x80);
        }

        private void ror(UInt16 address)
        {
            byte B = Peek(address);
            bool bit = flagset(C);
            setflag(C, (B & 0x01) == 0x01);
            B = (byte)(B >> 1);
            if(bit)
                B = (byte)(B | 0x80);
            Poke(address, B);
            setflag(Z, B == 0);
            setflag(N, B >= 0x80);
        }

        private void ror_A()
        {
            bool bit = flagset(C);
            setflag(C, (A & 0x01) == 0x01);
            A = (byte)(A >> 1);
            if(bit)
                A = (byte)(A | 0x80);
            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        private void rol_A()
        {
            bool bit = flagset(C);
            setflag(C, (A & 0x80) == 0x80);
            A = (byte)(A << 1);
            if (bit)
                A = (byte)(A | 0x01);
            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        private void rol(UInt16 address)
        {
            byte B = Peek(address);
            bool bit = flagset(C);
            setflag(C, (B & 0x80) == 0x80);
            B = (byte)(B << 1);
            if (bit)
                B = (byte)(B | 0x01);
            Poke(address, B);
            setflag(Z, B == 0);
            setflag(N, B >= 0x80);
        }

        private void cpx(UInt16 address)
        {
            UInt16 H = (UInt16)(X - Peek(address));
            setflag(C, H <= 0xFF);
            setflag(Z, lo(H) == 0);
            setflag(N, lo(H) >= 0x80);
        }

        private void cpy(UInt16 address)
        {
            UInt16 H = (UInt16)(Y - Peek(address));
            setflag(C, H <= 0xFF);
            setflag(Z, lo(H) == 0);
            setflag(N, lo(H) >= 0x80);
        }

        private void pla()
        {
            A = pull();
            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        private void lsr(UInt16 address)
        {
            byte B = Peek(address);
            setflag(C, (B & 0x01) == 0x01);
            B = (byte)(B >> 1);
            Poke(address, B);
            setflag(Z, B == 0);
            setflag(N, false);
        }

        private void lsr_A()
        {
            setflag(C, (A & 0x01) == 0x01);
            A = (byte)(A >> 1);
            setflag(Z, A == 0);
            setflag(N, false);
        }

        private void eor(UInt16 address)
        {
            A = (byte)(A ^ Peek(address));
            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        private void bit(UInt16 address)
        {
            UInt16 H = (UInt16)Peek(address);
            setflag(N, (H & 0x80) == 0x80);
            setflag(V, (H & 0x40) == 0x40);
            setflag(Z, (H & A) == 0);
        }

        private void asl_A()
        {
            setflag(C, (A & 0x80) == 0x80);
            A = (byte)(A << 1);
            setflag(Z, A == 0);
            setflag(N, A >= 0x80);
        }

        private void asl(UInt16 address)
        {
            byte B = Peek(address);
            setflag(C, (B & 0x80) == 0x80);
            B = (byte)(B << 1);
            Poke(address, B);
            setflag(Z, B == 0);
            setflag(N, B >= 0x80);
        }

        private void dec(UInt16 address)
        {
            UInt16 H = (UInt16)(Peek(address) - 1);
            Poke(address, (byte)H);
            setflag(Z,lo(H) == 0);
            setflag(N,lo(H) >= 0x80);
        }

        private void irq()
        {
            if(!flagset(I))
            {
                setflag(B, false);
                push(hi(PC));
                push(lo(PC));
                push(P);
                setflag(I, true);
                PC = PeekWord(0xFFFE);
            }
        }

        private void rti()
        {
            P = pull();
            PC = pull();
            PC += (UInt16)(pull() *256);
        }

        #endregion

        public void NonMaskableInterrupt()
        {
            setflag(B, false);
            push(hi(PC));
            push(lo(PC));
            push(P);
            setflag(I, true);
            PC = PeekWord(0xFFFA);
        }

        public bool IsBitSet(UInt16 address, int bit)
        {
            bool ret = (Peek(address) & (1 << bit)) == (1 << bit);
            return ret;
        }

        public void SetBit(UInt16 address, int bit)
        {
            Poke(address, (byte)(Peek(address) | (1 << bit)));
        }

        public void UnsetBit(UInt16 address, int bit)
        {
            Poke(address, (byte)(Peek(address) & (0xff - (1 << bit))));
        }

        public byte ColourRam(int offset)
        {
            return _memoryIo.ColorRam(offset);
        }

        public UInt16 ScreenAddress
        {
            get { return _memoryIo.ScreenAddress; }
        }

        public bool CharRomBitSet(byte value, int x, int y)
        {
            return _memoryIo.CharRomBitSet(value, x, y);
        }

        public UInt16 VicBankAddress
        {
            get { return _memoryIo.BankAddress; }
        }

        public UInt16 VicWatchTextFrom
        {
            get { return _memoryIo.WatchVicFrom; }
        }

        public UInt16 VicWatchTextTo
        {
            get { return _memoryIo.WatchVicTo; }
        }

        private byte hi(UInt16 value)
        {
            return (byte)(value >> 8);
        }
            
        private byte lo(UInt16 value)
        {
            return (byte)(value & ((2 << 16) - 1));
        }
    }
}
