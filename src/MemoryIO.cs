using System;
using System.IO;
using System.Reflection;

namespace C64
{
    public class MemoryIO
    {
        private readonly byte[] _basicRom = new byte[0x2000];
        private readonly byte[] _charRom = new byte[0x1000];
        private readonly byte[] _colorRam = new byte[0x0400];
        private readonly byte[] _kernalRom = new byte[0x2000];
        private readonly byte[] _vicRegs = new byte[0x002f];
        private UInt16 _charRomAddress;
        private bool _charen, _hiAndLo, _hiOrLo;
        private bool _hiram;
        private bool _loram;
        private UInt16 _screenAddress;

        public MemoryIO()
        {
            Ram = new byte[0x10000];
            unsafe
            {
                fixed (byte* pRam = Ram, pColor = _colorRam)
                {
                    var rand = new Random();

                    // Initialize RAM with powerup pattern
                    byte* p = pRam;
                    for (int i = 0; i < 65536; i++)
                        *p++ = (byte) (rand.Next() & 0xff);

                    // Initialize color RAM with random values
                    p = pColor;
                    for (int i = 0; i < 1024; i++)
                        *p++ = (byte) (rand.Next() & 0x0f);
                }
            }

            string exePath = Assembly.GetExecutingAssembly().Location;
            exePath = exePath.Substring(0, exePath.LastIndexOf("\\", StringComparison.Ordinal));
            LoadRom(_kernalRom, Path.Combine(exePath, "..\\..\\..\\rom\\kernal.rom"));
            LoadRom(_basicRom, Path.Combine(exePath, "..\\..\\..\\rom\\basic.rom"));
            LoadRom(_charRom, Path.Combine(exePath, "..\\..\\..\\rom\\char.rom"));
            Poke(0, 0xff);
            Poke(1, 0xff);
            Poke(0xDC00, 0x0f);
            Poke(0xDC01, 0x0f);
        }

        public ushort BankAddress { get; private set; }

        public byte[] Ram { get; set; }

        public ushort WatchVicFrom { get; private set; }

        public ushort WatchVicTo { get; private set; }

        public byte[] Stack
        {
            get
            {
                var stack = new byte[0x100];
                Array.ConstrainedCopy(Ram, 0x100, stack, 0, 0x100);
                return (byte[]) stack.Clone();
            }
        }

        public UInt16 ScreenAddress
        {
            get { return (UInt16) (BankAddress + _screenAddress); }
        }

        public byte ColorRam(int offset)
        {
            return _colorRam[offset];
        }

        private byte ColourRead(UInt16 address)
        {
            return _colorRam[address - 0xD800];
        }

        private void ColourWrite(UInt16 address, byte color)
        {
            _colorRam[address - 0xD800] = color;
        }

        public bool CharRomBitSet(byte value, int x, int y)
        {
            return (VicPeek((UInt16)(BankAddress + _charRomAddress + value * 8 + y)) & (0x80 >> x)) == (0x80 >> x);
        }

        public byte Peek(UInt16 address)
        {
            if (address >= 0xA000 && address <= 0xBFFF)
            {
                if (_hiAndLo)
                    return _basicRom[address - 0xA000];
                return Ram[address];
            }
            if (address >= 0xD000 && address <= 0xDFFF)
            {
                if (_hiOrLo)
                {
                    if (_charen)
                    {
                        return IoIn(address);
                    }
                    return _charRom[address - 0xD000];
                }
                return Ram[address];
            }
            if (address >= 0xE000 && address <= 0xFFFF)
            {
                if (_hiram)
                {
                    return _kernalRom[address - 0xE000];
                }
                return Ram[address];
            }
            return Ram[address];
        }

        public void Poke(UInt16 address, byte value)
        {
            Ram[address] = value;
            if (address == 0x0001)
            {
                IoPort(value);
            }
            else if (address >= 0xD000 && address <= 0xDFFF)
            {
                if (_hiOrLo)
                    IoOut(address, value);
            }
        }

        public byte VicPeek(UInt16 address)
        {
            if (address >= 0x1000 && address <= 0x1FFF)
                return _charRom[address - 0x1000];
            if (address >= 0x9000 && address <= 0x9FFF)
                return _charRom[address - 0x9000];
            return Ram[address];
        }

        private byte VicRead(UInt16 address)
        {
            if (address == 0xD012)
                return 0x00;
            return _vicRegs[address - 0xD000];
        }

        private void VicWrite(UInt16 address, byte value)
        {
            _vicRegs[address - 0xD000] = value;
            if (address == 0xD018)
            {
                _screenAddress = (UInt16) ((value >> 4)*0x0400);
                _charRomAddress = (UInt16) ((value & 0x0E)*0x0400);
                SetVicWatchRange((UInt16) (BankAddress + _screenAddress), (UInt16) (BankAddress + _screenAddress + 999));
            }
        }

        private void VicBank(byte val)
        {
            BankAddress = (UInt16) ((3 - (val & 3))*0x4000);
            SetVicWatchRange((UInt16) (BankAddress + _screenAddress), (UInt16) (BankAddress + _screenAddress + 999));
        }

        private void SetVicWatchRange(UInt16 startAddress, UInt16 endAddress)
        {
            WatchVicFrom = startAddress;
            WatchVicTo = endAddress;
        }

        private void IoPort(byte port)
        {
            var maskedPort = (byte) (port & Ram[0]);
            _loram = (maskedPort & 1) == 1;
            _hiram = (maskedPort & 2) == 2;
            _charen = (maskedPort & 4) == 4;
            _hiAndLo = _hiram && _loram;
            _hiOrLo = _hiram || _loram;
        }

        private byte IoIn(UInt16 address)
        {
            if (address >= 0xD000 && address <= 0xD02E)
                return VicRead(address);
            if (address >= 0xD800 && address <= 0xDBFF)
                return ColourRead(address);
            return 0xFF;
        }

        private void IoOut(UInt16 address, byte value)
        {
            if (address >= 0xD000 && address <= 0xD02E)
                VicWrite(address, value);
            if (address == 0xDD00)
                VicBank(value);
            if (address >= 0xD800 && address <= 0xDBFF)
                ColourWrite(address, value);
        }

        private static void LoadRom(byte[] rom, string filename)
        {
            FileStream fs = File.Open(filename, FileMode.Open);
            var stream = new BinaryReader(fs);

            int offset = 0;
            int remaining = rom.Length;
            while (remaining > 0)
            {
                int read = stream.Read(rom, offset, remaining);
                if (read <= 0)
                {
                    throw new EndOfStreamException(String.Format("End of stream reached with {0} bytes left to read",
                        remaining));
                }
                remaining -= read;
                offset += read;
            }
            stream.Close();
            fs.Close();
        }
    }
}