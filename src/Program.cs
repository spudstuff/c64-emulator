﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Primitives;
using SdlDotNet.Input;
using System.Drawing;
using Font = SdlDotNet.Graphics.Font;

namespace C64
{
    public class Program
    {
        private const int ScaleX = 3;
        private const int ScaleY = 3;
        private const int BorderOffsetX = 100;
        private const int BorderOffsetY = 125;
        private string _autorun;
        private readonly Surface _screen;
        private readonly Font _font;
        private readonly C64 _c64 = new C64();
        private readonly Color[] _c64StandardColours = { Color.FromArgb(0x00, 0x00, 0x00),
                                                         Color.FromArgb(0xFF, 0xFF, 0xFF),
                                                         Color.FromArgb(0x68, 0x37, 0x2B),
                                                         Color.FromArgb(0x70, 0xA4, 0xB2),
                                                         Color.FromArgb(0x6F, 0x3D, 0x86),
                                                         Color.FromArgb(0x58, 0x8D, 0x43),
                                                         Color.FromArgb(0x35, 0x28, 0x79),
                                                         Color.FromArgb(0xB8, 0xC7, 0x6F),
                                                         Color.FromArgb(0x6F, 0x4F, 0x25),
                                                         Color.FromArgb(0x43, 0x39, 0x00),
                                                         Color.FromArgb(0x9A, 0x67, 0x59),
                                                         Color.FromArgb(0x44, 0x44, 0x44),
                                                         Color.FromArgb(0x6C, 0x6C, 0x6C),
                                                         Color.FromArgb(0x9A, 0xD2, 0x84),
                                                         Color.FromArgb(0x6C, 0x5E, 0xB5),
                                                         Color.FromArgb(0x95, 0x95, 0x95) };
        byte[] _KeyMatrix = new byte[8];	// C64 keyboard matrix, 1 bit/key (0: key down, 1: key up)
        byte[] _RevMatrix = new byte[8];	// Reversed keyboard matrix     
        byte joykey;			            // Joystick keyboard emulation mask value
        byte _Joystick1;	                // Joystick 1 AND value     
        byte _Joystick2;	                // Joystick 2 AND value
        byte[,] collisionsBlank = new byte[513, 280];

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            new Program(args.Length > 0 ? args[0] : null).Go();
        }

        public Program(string autorunProg)
        {
            _autorun = autorunProg;
            _screen = Video.SetVideoMode(BorderOffsetX * 2 + (320 * ScaleX), BorderOffsetY * 2 + (200 * ScaleY) + 21, 24, false, false, false, true);
            Video.WindowCaption = "C64 Emulator by Spud";
            _font = new Font("c:\\windows\\fonts\\consola.ttf", 15);
            _c64.Trace = true;
            _c64.OnCpuError += c64_onCPUError;
            for (int i = 0; i < 8; i++)
            {
                _KeyMatrix[i] = _RevMatrix[i] = 0xff;
            }
        }

        void c64_onCPUError(string message)
        {
            _screen.Draw(new Box(new Point(0, 0), new Point(BorderOffsetX * 2 + (320 * ScaleX), 20)), Color.Red, false, true);
            Surface sur = _font.Render(message, Color.Yellow, true);
            _screen.Blit(sur, new Point(5, 3));
            _screen.Update();
        }

        public void Go()
        {
            _c64.Start();
            Events.Quit += new EventHandler<QuitEventArgs>(Quit);
            Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(KeyDown);
            Events.KeyboardUp += new EventHandler<KeyboardEventArgs>(KeyUp);
            Events.Tick += new EventHandler<TickEventArgs>(Events_Tick);
            Events.Fps = 60;
            Events.Run();
        }

        void Events_Tick(object sender, TickEventArgs e)
        {
            if (!_c64.NextInstructions())
            {
                return;
            }

            if (_autorun != null)
            {
                _c64.ResetAndLoadPrg(_autorun, false, true);
                _autorun = null;
            }

            //PollKeyboard(_KeyMatrix, _RevMatrix, ref joykey);

            // Screen blanked?
            byte collisionMask = 0;
            if (_c64.IsBitSet(0xD011, 4))
            {
                // Multicolor character mode?
                bool multiColorCharacters = _c64.IsBitSet(0xD016, 4);

                // Character mode - draw character screen
                for (UInt16 pos = 0; pos <= 999; pos++)
                {
                    int x0 = (pos % 40) * 8;
                    int y0 = (pos / 40) * 8;
                    if (!multiColorCharacters)// || (c64.ColourRam(pos) & (1 << 3)) == (1 << 3))
                    {
                        for (int y = 0; y < 8; y++)
                        {
                            for (int x = 0; x < 8; x++)
                            {
                                if (_c64.CharRomBitSet(_c64.Peek((UInt16)(_c64.ScreenAddress + pos)), x, y))
                                {
                                    int col = _c64.ColourRam(pos) & 15;
                                    _screen.Draw(new Box(new Point((x0 + x) * ScaleX + BorderOffsetX, (y0 + y) * ScaleY + BorderOffsetY), new Point((x0 + x) * ScaleX + ScaleX - 1 + BorderOffsetX, (y0 + y) * ScaleY + ScaleY - 1 + BorderOffsetY)), _c64StandardColours[col], true, true);
                                }
                                else
                                    _screen.Draw(new Box(new Point((x0 + x) * ScaleX + BorderOffsetX, (y0 + y) * ScaleY + BorderOffsetY), new Point((x0 + x) * ScaleX + ScaleX - 1 + BorderOffsetX, (y0 + y) * ScaleY + ScaleY - 1 + BorderOffsetY)), _c64StandardColours[_c64.Peek(0xD021) & 15], true, true);
                            }
                        }
                    }
                    else
                    {
                        for (int y = 0; y < 8; y++)
                        {
                            for (int x = 0; x < 8; x += 2)
                            {
                                bool lb = _c64.CharRomBitSet(_c64.Peek((UInt16)(_c64.ScreenAddress + pos)), x, y);
                                bool rb = _c64.CharRomBitSet(_c64.Peek((UInt16)(_c64.ScreenAddress + pos)), x + 1, y);
                                if (lb || rb)
                                {
                                    int color;
                                    if (!lb)
                                    {
                                        color = _c64.Peek(0xD022) & 15;
                                    }
                                    else if (!rb)
                                    {
                                        color = _c64.Peek(0xD023) & 15;
                                    }
                                    else
                                    {
                                        color = (_c64.ColourRam(pos) >> 3) & 15;
                                    }
                                    _screen.Draw(new Box(new Point((x0 + x) * ScaleX + BorderOffsetX, (y0 + y) * ScaleY + BorderOffsetY), new Point((x0 + x) * ScaleX + ScaleX + ScaleX - 1 + BorderOffsetX, (y0 + y) * ScaleY + ScaleY - 1 + BorderOffsetY)), _c64StandardColours[color], true, true);
                                }
                                else
                                    _screen.Draw(new Box(new Point((x0 + x) * ScaleX + BorderOffsetX, (y0 + y) * ScaleY + BorderOffsetY), new Point((x0 + x) * ScaleX + ScaleX + ScaleX - 1 + BorderOffsetX, (y0 + y) * ScaleY + ScaleY - 1 + BorderOffsetY)), _c64StandardColours[_c64.Peek(0xD021) & 15], true, true);
                            }
                        }
                    }
                }

                // Check sprites
                if (_c64.Peek(0xD015) != 0x00)
                {
                    byte[,] collisions = collisionsBlank;

                    // At least one sprite is on
                    for (int sprite = 7; sprite >= 0; sprite--)
                    {
                        if (_c64.IsBitSet(0xD015, sprite))
                        {
                            // Sprite is on -- get colour
                            int spriteCol = _c64.Peek((UInt16)(0xD027 + sprite)) & 0x0f;

                            // Expanded?
                            bool expandedH = _c64.IsBitSet(0xD01D, sprite);
                            bool expandedV = _c64.IsBitSet(0xD017, sprite);

                            // Multicolor?
                            bool multiColorSprites = _c64.IsBitSet(0xD01C, sprite);

                            // Position
                            int posX = _c64.Peek((UInt16)(0xD000 + (sprite * 2))) + (_c64.IsBitSet(0xD010, sprite) ? 256 : 0);
                            int posY = _c64.Peek((UInt16)(0xD001 + (sprite * 2)));

                            // Get sprite pointer
                            UInt16 spritePointer = (UInt16)(_c64.VicBankAddress + (UInt16)(_c64.Peek((UInt16)(0x07F8 + sprite)) * 64));

                            // Draw it
                            for (byte b = 0; b < 63; b++)
                            {
                                if (!multiColorSprites)
                                {
                                    // Hires sprite
                                    for (int i = 7; i >= 0; i--)
                                    {
                                        if (_c64.IsBitSet((UInt16)(spritePointer + b), i))
                                        {
                                            int xDraw = 28 + (posX * ScaleX) + ((((b % 3) * 8) + (7 - i)) * (expandedH ? ScaleX + ScaleX : ScaleX));
                                            int yDraw = -25 + (posY * ScaleY) + ((int)Math.Floor((double)b / 3.0d) * (expandedV ? ScaleY + ScaleY : ScaleY));
                                            _screen.Draw(new Box(new Point(xDraw, yDraw), new Point(xDraw + (expandedH ? ScaleX + ScaleX - 1 : ScaleX - 1), yDraw + (expandedV ? ScaleY + ScaleY - 1 : ScaleY - 1))), _c64StandardColours[spriteCol], true, true);
                                            collisions[posX, posY] = (byte)(collisions[posX, posY] | (byte)(Math.Pow(2, sprite)));
                                            if (expandedH) collisions[posX + 1, posY] = (byte)(collisions[posX + 1, posY] | (byte)(Math.Pow(2, sprite)));
                                            if (expandedV) collisions[posX, posY + 1] = (byte)(collisions[posX, posY + 1] | (byte)(Math.Pow(2, sprite)));
                                            if (expandedH && expandedV) collisions[posX + 1, posY + 1] = (byte)(collisions[posX + 1, posY + 1] | (byte)(Math.Pow(2, sprite)));
                                        }
                                    }
                                }
                                else
                                {
                                    // Multicolor sprite
                                    for (int i = 7; i >= 1; i -= 2)
                                    {
                                        bool lb = _c64.IsBitSet((UInt16)(spritePointer + b), i);
                                        bool rb = _c64.IsBitSet((UInt16)(spritePointer + b), i - 1);
                                        if (lb || rb)
                                        {
                                            int color;
                                            if (!lb)
                                            {
                                                color = _c64.Peek(0xD025) & 15;
                                            }
                                            else if (!rb)
                                            {
                                                color = spriteCol;
                                            }
                                            else
                                            {
                                                color = _c64.Peek(0xD026) & 15;
                                            }

                                            int xDraw = 28 + (posX * ScaleX) + ((((b % 3) * 8) + (7 - i)) * (expandedH ? ScaleX + ScaleX : ScaleX));
                                            int yDraw = -25 + (posY * ScaleY) + ((int)Math.Floor((double)b / 3.0d) * (expandedV ? ScaleY + ScaleY : ScaleY));
                                            _screen.Draw(new Box(new Point(xDraw, yDraw), new Point(xDraw + (expandedH ? 4 * ScaleX - 1 : 2 * ScaleX - 1), yDraw + (expandedV ? 2 * ScaleY - 1 : ScaleY - 1))), _c64StandardColours[color], true, true);
                                            collisions[posX, posY] = (byte)(collisions[posX, posY] | (byte)(Math.Pow(2, sprite)));
                                            if (expandedH) collisions[posX + 1, posY] = (byte)(collisions[posX + 1, posY] | (byte)(Math.Pow(2, sprite)));
                                            if (expandedV) collisions[posX, posY + 1] = (byte)(collisions[posX, posY + 1] | (byte)(Math.Pow(2, sprite)));
                                            if (expandedH && expandedV) collisions[posX + 1, posY + 1] = (byte)(collisions[posX + 1, posY + 1] | (byte)(Math.Pow(2, sprite)));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Scan for collisons
                    for (int x = 0; x < 320; x++)
                    {
                        for (int y = 0; y < 200; y++)
                        {
                            if (collisions[x, y] != 0)
                            {
                                collisionMask = (byte)(collisionMask | collisions[x, y]);
                            }
                        }
                    }
                    if (collisionMask != 0)
                    {
                        _c64.Poke(0xD01E, collisionMask);
                    }
                }
            }

            // Draw border
            _screen.Draw(new Box(new Point(0, 0), new Point(BorderOffsetX * 2 + (320 * ScaleX), BorderOffsetY - 1)), _c64StandardColours[_c64.Peek(0xD020) & 15], true, true);
            _screen.Draw(new Box(new Point(0, 0), new Point(BorderOffsetX - 1, BorderOffsetY * 2 + (200 * ScaleY))), _c64StandardColours[_c64.Peek(0xD020) & 15], true, true);
            _screen.Draw(new Box(new Point(0, BorderOffsetY + (200 * ScaleY)), new Point(BorderOffsetX * 2 + (320 * ScaleX), BorderOffsetY * 2 + (200 * ScaleY))), _c64StandardColours[_c64.Peek(0xD020) & 15], true, true);
            _screen.Draw(new Box(new Point(BorderOffsetX + (320 * ScaleX), 0), new Point(BorderOffsetX * 2 + (320 * ScaleX), BorderOffsetY * 2 + (200 * ScaleY))), _c64StandardColours[_c64.Peek(0xD020) & 15], true, true);

            // Get state of C64
            var state = _c64.GetState();
            _screen.Draw(new Box(new Point(0, BorderOffsetY * 2 + (200 * ScaleY)), new Point(BorderOffsetX * 2 + (320 * ScaleX), BorderOffsetY * 2 + (200 * ScaleY) + 20)), Color.Blue, false, true);
            Surface sur = _font.Render(String.Format("Instruction Register: {0:x2} - {1}    Accumulator: {2:x2}    X Register: {3:x2}    Y Register: {4:x2}    Stack Pointer: {5:x2}   Flags: {6}  Coll: {7}",
                                                     state.IR, state.Opcode, state.A, state.X, state.Y, state.S, state.Flags, collisionMask), Color.Yellow, true);
            _screen.Blit(sur, new Point(5, BorderOffsetY * 2 + (200 * ScaleY) + 3));
            _screen.Update();
        }

        private void KeyUp(object sender, KeyboardEventArgs e)
        {
            // Normal keys
            if ((e.Mod & ModifierKeys.LeftShift) == 0 && (e.Mod & ModifierKeys.RightShift) == 0)
            {
                switch (e.Key)
                {
                    case Key.F9: Fire(false); return;
                }
            }
        }

        private void KeyDown(object sender, KeyboardEventArgs e)
        {
            byte ch = (byte)(e.KeyboardCharacter.ToUpper().ToCharArray()[0]);
            byte matrix = 64; // no key pressed
            byte shift = 0;
            byte ctrl = 0;
            byte commodore = 0;

            // Eat special keys and return immediately
            switch (e.Key)
            {
                case Key.LeftShift: shift = 1; break;
                case Key.RightShift: shift = 1; break;
                case Key.LeftAlt: commodore = 2; break;
                case Key.RightAlt: commodore = 2; break;
                case Key.LeftControl:
                    ctrl = 4;
                    _c64.Poke(0xDC00, 15);
                    _c64.Poke(0xDC01, 15);
                    Debug.WriteLine("FIRE");
                    break;
                case Key.RightControl: ctrl = 4; break;
                case Key.LeftMeta: return;
                case Key.Menu: return;
                case Key.Tab: return;
                case Key.CapsLock: return;
                case Key.LeftWindows: return;
                case Key.RightWindows: return;
            }

            if (shift != 0 || ctrl != 0 || commodore != 0)
            {
                _c64.Poke(0x28D, (byte)(shift & ctrl & commodore));
                return;
            }

            // Normal keys
            if ((e.Mod & ModifierKeys.LeftShift) == 0 && (e.Mod & ModifierKeys.RightShift) == 0)
            {
                switch (e.Key)
                {
                    case Key.Backspace: ch = 20; break;
                    case Key.Return: ch = 13; matrix = 1; break;
                    case Key.KeypadEnter: ch = 13; matrix = 1; break;
                    case Key.Space: ch = 32; matrix = 60; break;
                    case Key.Insert: ch = 148; matrix = 0; break;
                    case Key.UpArrow: ch = 145; matrix = 54; break;
                    case Key.DownArrow: ch = 17; matrix = 7; break;
                    case Key.LeftArrow: ch = 157; matrix = 57; break;
                    case Key.RightArrow: ch = 29; matrix = 2; break;
                    case Key.Escape: ch = 0; matrix = 63; break;
                    case Key.F8: _c64.NonMaskableInterrupt(); return;
                    case Key.F9: Fire(true); return;
                    case Key.F10: _c64.Reset(); return;
                    case Key.F11:
                    case Key.F12:
                        var dialog = new OpenFileDialog();
                        dialog.Filter = "C64 files|*.PRG;*.P00|All files (*.*)|*.*";
                        dialog.InitialDirectory = "programs\\";
                        dialog.Title = "Select a program";
                        if (dialog.ShowDialog() == DialogResult.OK)
                        {
                            _c64.ResetAndLoadPrg(dialog.FileName, false, e.Key == Key.F12);
                        }
                        return;
                }
            }
            else
            {
                switch (e.Key)
                {
                    case Key.One: ch = (byte)'!'; break;
                    case Key.Two: ch = (byte)'@'; break;
                    case Key.Three: ch = (byte)'#'; break;
                    case Key.Four: ch = (byte)'$'; break;
                    case Key.Five: ch = (byte)'%'; break;
                    case Key.Six: ch = (byte)'^'; break;
                    case Key.Seven: ch = (byte)'&'; break;
                    case Key.Eight: ch = (byte)'*'; break;
                    case Key.Nine: ch = (byte)'('; break;
                    case Key.Zero: ch = (byte)')'; break;
                    case Key.Semicolon: ch = (byte)':'; break;
                    case Key.Quote: ch = (byte)'"'; break;
                    case Key.Slash: ch = (byte)'?'; break;
                    case Key.Comma: ch = (byte)'<'; break;
                    case Key.Period: ch = (byte)'>'; break;
                    case Key.Equals: ch = (byte)'+'; break;
                    case Key.Backspace: ch = 20; break;
                    case Key.Return: ch = 13; matrix = 1; break;
                    case Key.KeypadEnter: ch = 13; matrix = 1; break;
                    case Key.Space: ch = 32; matrix = 60; break;
                    case Key.Insert: ch = 148; matrix = 0; break;
                    case Key.UpArrow: ch = 145; matrix = 54; break;
                    case Key.DownArrow: ch = 17; matrix = 7; break;
                    case Key.LeftArrow: ch = 157; matrix = 57; break;
                    case Key.RightArrow: ch = 29; matrix = 2; break;
                }
            }

            switch ((char)ch)
            {
                case 'A': matrix = 10; break;
                case 'B': matrix = 28; break;
                case 'C': matrix = 20; break;
                case 'D': matrix = 18; break;
                case 'E': matrix = 14; break;
                case 'F': matrix = 21; break;
                case 'G': matrix = 26; break;
                case 'H': matrix = 29; break;
                case 'I': matrix = 33; break;
                case 'J': matrix = 34; break;
                case 'K': matrix = 37; break;
                case 'L': matrix = 42; break;
                case 'M': matrix = 36; break;
                case 'N': matrix = 39; break;
                case 'O': matrix = 38; break;
                case 'P': matrix = 41; break;
                case 'Q': matrix = 62; break;
                case 'R': matrix = 17; break;
                case 'S': matrix = 13; break;
                case 'T': matrix = 22; break;
                case 'U': matrix = 30; break;
                case 'V': matrix = 31; break;
                case 'W': matrix = 9; break;
                case 'X': matrix = 23; break;
                case 'Y': matrix = 25; break;
                case 'Z': matrix = 12; break;
                case '0': matrix = 35; break;
                case '1': matrix = 56; break;
                case '2': matrix = 59; break;
                case '3': matrix = 8; break;
                case '4': matrix = 11; break;
                case '5': matrix = 16; break;
                case '6': matrix = 19; break;
                case '7': matrix = 24; break;
                case '8': matrix = 27; break;
                case '9': matrix = 32; break;
                case '+': matrix = 40; break;
                case '-': matrix = 43; break;
                case '.': matrix = 44; break;
                case ':': matrix = 45; break;
                case '@': matrix = 46; break;
                case ',': matrix = 47; break;
                case '*': matrix = 49; break;
                case ';': matrix = 50; break;
                case '=': matrix = 53; break;
                case '/': matrix = 55; break;
            }

            byte buff = _c64.Peek(0xC6);
            if (buff < _c64.Peek(0x289))
            {
                _c64.Poke(0xCB, matrix);
                _c64.Poke((UInt16)(0x0277 + buff), ch);
                _c64.Poke(0xC6, (byte)(buff + 1));
            }
        }

        private void Fire(bool down)
        {
            if (down)
            {
                _c64.UnsetBit(0xdc01, 4);
                _c64.UnsetBit(0xdc00, 4);
            }
            else
            {
                _c64.SetBit(0xdc01, 4);
                _c64.SetBit(0xdc00, 4);
            }
        }

        private void Quit(object sender, QuitEventArgs e)
        {
            Events.QuitApplication();
        }
    }
}