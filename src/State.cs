﻿using System;

namespace C64
{
    public class State
    {
        private readonly string padding = "00000000";
        public UInt16 PC;
        public byte IR;
        public byte A;
        public byte X;
        public byte Y;
        public byte S;
        public byte P;

        public string Flags
        {
            get
            {
                string val = Convert.ToString(P, 2);
                return val.Length < 8 ? padding.Substring(0, 8-val.Length) + val : val;
            }
        }

        public string Opcode
        {
            get
            {
                return String.Format("{0} ({1})", C64.Opcodes[IR, 0], C64.Opcodes[IR, 1]);
            }
        }
    }
}
